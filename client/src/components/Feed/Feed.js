import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import profile_pic from './niki.jpg';
import image from './Image 4.png';
import './Feed.css';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'

const Feed = ({match}) => {
    
    const [feedPosts, getFeedPostInfo] = useState({});

    useEffect(() => { const fetchFeed = async () => {
        const result = await fetch(`http://localhost:4000/api/feed`)
        const body = await result.json();
        const posts = body;
        console.log(body);
        const postItems = posts.map((posts) =>
            <div class="border shadow-lg p-3 mb-5 bg-white rounded">
                <body class="card mt-3">
                    <div class="card-body">
                        {/* PROFILE PIC */}
                        <div class="row ">
                            <div class="col-5">
                                <img className=" rounded feed_profile_pic row" src={profile_pic} alt="" />
                                <span className="username row"> {posts.user_name}  </span>
                                <span className="title row"> <i> works in </i>  <p className="comp"> {posts.company} </p></span> 
                            </div>
                        </div>

                        {/* image */}
                        <div className="shadow-sm p-3 mb-5 bg-white rounded">
                            <img className="rounded feed_image rounded mx-auto d-block" src={image} alt=""/>
                            {/* <img src={posts.image_path} alt="" className="rounded image rounded mx-auto d-block"/> */}
                        </div>

                        {/* TAGS */}
                        <div className="">
                            <span class="col-12 row">{posts.text}</span>
                            <ul class="hashtags"> 
                               <li> <a href="#">#{posts.tags[0]}</a> </li>
                               <li> <a href="#">#{posts.tags[1]}</a> </li>
                               <li> <a href="#">#{posts.tags[2]}</a> </li>
                            </ul>
                        </div>

                        {/* TIMESTAMP */}
                        <div className="timestamp">
                            {posts.time_stamp}
                        </div>
                    
                    </div>
                </body>
            </div>
        );
        console.log(postItems[0]);
        getFeedPostInfo(postItems);
    }

    fetchFeed();
    },[]);
    return (
        <>
            {Object.keys(feedPosts).map(function(key) {
                return <div>{feedPosts[key]}</div>;
            })}
        </>
    )
}

export default Feed;