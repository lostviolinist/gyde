import React from 'react';
import logo from './logo.png';
import './LandingPage.css'




const LandingPage = props => (
    <> 
        <body class="landing_container border landingPage">
            <img className="landing_image" src={logo} alt=""/>
        </body>
    </>

);

export default LandingPage;